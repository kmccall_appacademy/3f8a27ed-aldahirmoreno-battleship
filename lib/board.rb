class Board
  attr_reader :grid

  def self.default_grid
    @grid = [[nil,nil,nil,nil,nil,nil,nil,nil,nil,nil],[nil,nil,nil,nil,nil,nil,nil,nil,nil,nil],[nil,nil,nil,nil,nil,nil,nil,nil,nil,nil],[nil,nil,nil,nil,nil,nil,nil,nil,nil,nil],[nil,nil,nil,nil,nil,nil,nil,nil,nil,nil],[nil,nil,nil,nil,nil,nil,nil,nil,nil,nil],[nil,nil,nil,nil,nil,nil,nil,nil,nil,nil],[nil,nil,nil,nil,nil,nil,nil,nil,nil,nil],[nil,nil,nil,nil,nil,nil,nil,nil,nil,nil],[nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]]
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def count
    grid.flatten.count(:s)
  end

  def empty?(pos = false)
    if pos == false && count != 0
      return false
    elsif pos == false && count == 0
      return true
    elsif grid[pos[0]][pos[1]] == nil
      true
    else
      false
    end
  end

  def full?
    return true if grid.flatten.count(nil) == 0
    false
  end

  def place_random_ship
    num = grid.length
    first_index = rand(num)
    second_index = rand(num)
    raise "The board is full" if full?
    # if count == 0
    #   while full? == false
    #     if empty?([first_index,second_index])
    #       grid[first_index][second_index] = :s
    #     end
    #     first_index = rand(num)
    #     second_index = rand(num)
    #   end
    # else
    #   grid[first_index][second_index] = :s
    # end
    grid[first_index][second_index] = :s
  end

  def won?
    if count == 0
      return true
    else
      return false
    end
  end

end
