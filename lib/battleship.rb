class BattleshipGame
  attr_reader :board, :player, :board_tot
  attr_writer :board

  def initialize(player, board)
    @board_tot = board
    @board = board.grid
    @player = player
  end

  def attack(pos)
    board[pos[0]][pos[1]] = :x
  end

  def count
    board_tot.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    pos = player.get_play
    attack(pos)
  end

end
